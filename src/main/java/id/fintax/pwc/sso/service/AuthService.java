package id.fintax.pwc.sso.service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;


public interface AuthService {

    String getPWCLoginURL(HttpServletResponse response);

    String getPWCLogoutURL();

    Map<String, String> getUserDetail(String accessCode) throws IOException, InterruptedException;

    String getAccessToken(String accessCode) throws IOException, InterruptedException;

    boolean isAccessCodeValid(String accessCode) throws IOException, InterruptedException;
}
