package id.fintax.pwc.sso.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.fintax.pwc.sso.model.UserModel;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.net.URLEncoder;
//import java.net.http.HttpClient;
//import java.net.http.HttpRequest;
//import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Component
public class AuthServiceImpl implements AuthService {

    @Value("${fintax.app.client_id}")
    private String clientId;

    @Value("${fintax.app.client_secret}")
    private String clientSecret;

    @Value("${fintax.app.OFIS_AUTH_URL}")
    private String ofisAuthURL;

    @Value("${fintax.app.OFIS_ACCESS_URL}")
    private String ofisAccessURL;

    @Value("${fintax.app.OFIS_USER_PROFILE_URL}")
    private String ofisUserProfileURL;

    @Value("${fintax.app.signout_url}")
    private String signOutURL;

    @Value("${fintax.app.REDIRECT_URL}")
    private String redirectURL;

    @Override
    public String getPWCLoginURL(HttpServletResponse response) {
        String uuid = UUID.randomUUID().toString();
        String scope = "openid";
        Cookie stateCookie = new Cookie("OAuth2State", uuid);
        response.addCookie(stateCookie);
        String finalURL = ofisAuthURL + "?response_type=code&client_id=" + clientId + "&redirect_uri=" + redirectURL + "&scope=" + scope + "&state=" + uuid;

        return finalURL;

    }

    @Override
    public String getPWCLogoutURL() {
        return signOutURL;
    }

    @Override
    public Map<String, String> getUserDetail(String accessToken) throws IOException, InterruptedException {
        Map<String, String> result = new HashMap<>();
        if (accessToken == null) {
            result.put("message", "Your Access Token is Invalid");
            return result;
        }
        System.setProperty("jdk.tls.client.protocols", "TLSv1.2");
        System.out.println("Redirect URL: " + redirectURL);

        String encodedAccessToken = URLEncoder.encode(accessToken, StandardCharsets.UTF_8.toString());

        String url = ofisUserProfileURL + "?access_token=" + encodedAccessToken;
        CloseableHttpClient client = HttpClients.createDefault();
        HttpGet request = new HttpGet(url);
        CloseableHttpResponse response = client.execute(request);
        try {
            System.out.println(response.getStatusLine().toString());

            HttpEntity entity = response.getEntity();
            if (entity != null) {
                result = convertStringToMap(EntityUtils.toString(entity));
            }
        } finally {
            response.close();
        }

        client.close();
//        HttpClient client = HttpClient.newHttpClient();
//        HttpRequest request = HttpRequest.newBuilder()
//                .uri(URI.create(url))
//                .GET()
//                .build();
//        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
//        result = convertStringToMap(response.body());
        return result;
    }

    @Override
    public String getAccessToken(String accessCode) throws IOException, InterruptedException {
        Map<String, String> result = connectToOFISAuth(accessCode);
        String accessToken = result.get("access_token");
        return accessToken;
    }

    @Override
    public boolean isAccessCodeValid(String accessCode) throws IOException, InterruptedException {
        boolean result = true;
        Map<String, String> response = connectToOFISAuth(accessCode);
        String accessToken = response.get("access_token");
        if (accessToken == null) {
            result = false;
            return result;
        }
        return result;
    }

    public Map<String, String> connectToOFISAuth(String accessCode) throws IOException, InterruptedException {
        System.setProperty("jdk.tls.client.protocols", "TLSv1.2");
        HashMap<String, String> postData = new HashMap<String, String>();
        postData.put("code", accessCode);
        postData.put("client_id", clientId);
        postData.put("client_secret", clientSecret);
        postData.put("redirect_uri", redirectURL);
        postData.put("grant_type", "authorization_code");
        ObjectMapper objectMapper = new ObjectMapper();
        String requestBody = objectMapper.writeValueAsString(postData);
        HttpPost post = new HttpPost(ofisAccessURL);
        post.addHeader("Content-Type", "application/json");

        post.setEntity(new StringEntity(requestBody));

        Map<String, String> result = new HashMap<String, String>();
        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(post)) {

             result = convertStringToMap(EntityUtils.toString(response.getEntity()));
        }
//        HttpClient client = HttpClient.newHttpClient();
//        HttpRequest request = HttpRequest.newBuilder()
//                .uri(URI.create(ofisAccessURL))
//                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
//                .setHeader("Content-Type", "application/json")
//                .build();
//        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
//        System.out.println(response.body());
//        Map<String, String> result = convertStringToMap(response.body());
        System.out.println(result.get("id_token"));
        return result;
    }

    private Map<String, String> convertStringToMap (String data) {
        Map<String, String> result = new HashMap<>();
        data = data.substring(1, data.length()-1);
        data = data.replaceAll("\"", "");
        String[] keyValuePairs = data.split(",");
        for(String pair : keyValuePairs)
        {
            String[] entry = pair.split(":");
            result.put(entry[0].trim(), entry[1].trim());
        }
        return result;
    }
}