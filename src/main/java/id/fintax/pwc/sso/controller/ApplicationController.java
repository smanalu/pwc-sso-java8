package id.fintax.pwc.sso.controller;

import id.fintax.pwc.sso.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("api/v1/auth/")
public class ApplicationController {

    @Autowired
    AuthService authService;

    @GetMapping(value = "login")
    public ModelAndView redirectToPWCLogin(HttpServletResponse response) {
        return new ModelAndView("redirect:" + authService.getPWCLoginURL(response));
    }

    @GetMapping(value = "logout")
    public ModelAndView redirectToPWCLogout() {
        return new ModelAndView("redirect:" + authService.getPWCLogoutURL());
    }

    @GetMapping(value = "user-detail")
    public Map<String, String> getUserDetail(@RequestParam(name = "code") String accessCode,
                              @RequestParam(name = "state") String state,
                              @CookieValue(value = "OAuth2State") String cookie) throws IOException, InterruptedException {
        System.out.println("State from URL: " + state);
        System.out.println("OAuth2State frot Cookies :" + cookie);
        String accessToken = authService.getAccessToken(accessCode);
        System.out.println("Access Token: " + accessToken);
        Map<String, String> response = authService.getUserDetail(accessToken);
        System.out.println(response);
        return response;
    }

    @GetMapping(value = "isAccessCodeValid")
    public Map<String, String> isAccessCodeValid(@RequestParam(name = "code") String accessCode) throws IOException, InterruptedException {
        boolean isValid = authService.isAccessCodeValid(accessCode);
        Map<String, String> result = new HashMap<>();
        result.put("isAccessCodeValid", Boolean.toString(isValid));
        return result;
    }
}